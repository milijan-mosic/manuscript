+++
title = 'About'
date = 2023-09-12T01:26:09+02:00
draft = false
+++

Hello there! I'm Milijan Mosic, a passionate software engineer with a deep love for feline companions, philosophical musings, and an unwavering interest in solarpunk—a brighter and greener future.

By day, I'm coding my way through the digital realm, crafting innovative solutions and pushing the boundaries of technology. But when the screen goes dark, you'll find me immersed in the world of cats, cherishing every moment with my whiskered friends. 🐾

Philosophy isn't just a hobby; it's a way of life. I enjoy pondering life's profound questions, seeking wisdom in ancient texts, and exploring the depths of human thought. 🤔

And then there's solarpunk—a vision of a sustainable, eco-friendly future where nature and technology coexist harmoniously. I'm intrigued by this movement and actively contribute my thoughts and efforts to bring it closer to reality. ☀️
