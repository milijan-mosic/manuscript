+++
title = 'Plans for the rest of the 2023'
date = 2023-09-11T22:58:27+02:00
draft = false
+++

## Yoooooooo!

With this post I'm officially starting my blog. After 10+ years of picking the "best" technology for it (which is really stupid, as you can see) I'm finally starting it.

## Schedule 

My plans for this year are something like this:

- *September*: making the project for my diplomma (full-stack blogging platform for journalists)
- *October*: going through CS50 course and getting certification
- *November*: using my laptop as server and learning about CICD (AKA making homelab)
- *December*: going through Advent of Code 2023 with Rust/Go.

## Final word

Now when I wrote it, I can see the pattern... one month is hands-on experience, the other one is something like "month of theory"... strange, but cool.

'Happy learning' == 'Hello world', so... happy learning!

